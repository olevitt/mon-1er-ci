package fr.levitt.tp4.tp4web.core;

import java.util.List;

public class TableauAffichage {

    private List<Departure> departures;

    public List<Departure> getDepartures() {
        return departures;
    }

    public void setDepartures(List<Departure> departures) {
        this.departures = departures;
    }
}
