package fr.levitt.tp4.tp4web.service;

import fr.levitt.tp4.tp4web.core.Gare;
import fr.levitt.tp4.tp4web.core.Gares;
import fr.levitt.tp4.tp4web.core.TableauAffichage;
import fr.levitt.tp4.tp4web.core.Train;

import java.util.List;

public interface GareService {

    public TableauAffichage getTableauDeparts(String idGare);
    public Gares getGares();
}
