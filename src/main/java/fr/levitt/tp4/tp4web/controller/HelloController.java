package fr.levitt.tp4.tp4web.controller;

import fr.levitt.tp4.tp4web.core.Gares;
import fr.levitt.tp4.tp4web.core.TableauAffichage;
import fr.levitt.tp4.tp4web.core.Train;
import fr.levitt.tp4.tp4web.service.GareService;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
public class HelloController {

    @Value("${exemple}")
    private String exemple;

    @Autowired
    private GareService gareService;

    @GetMapping("/gare/{id}")
    public TableauAffichage getGare(@PathVariable("id") String idGare) {
        return gareService.getTableauDeparts(idGare);
    }

    @GetMapping("/gare")
    public Gares getGares() {
        return gareService.getGares();
    }

    @GetMapping(value = "/sncf")
    public String sncf() throws IOException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("https://api.navitia.io/v1/coverage/sncf/stop_areas/stop_area%3AOCE%3ASA%3A87444000/departures?from_datetime=20200303T144459&")
                .header("Authorization","Basic MDg1OWU0NTQtMWExNi00MWJiLWJjYWItOWQwMzRmMDU5NGMyOjA4NTllNDU0LTFhMTYtNDFiYi1iY2FiLTlkMDM0ZjA1OTRjMg==")
                .build();

        try {
            Response response = client.newCall(request).execute();
            System.out.println(response.code());
            return response.body().string();
        }
        catch (IOException e) {
            System.out.println("Erreur réseau");
            e.printStackTrace();
            throw new IOException("Erreur réseau");
        }
    }

    @GetMapping(value = "/train")
    public Train hello() {
        System.out.println(exemple);
        Train train = new Train();
        train.setDestination("Labas");
        train.setNumero(42);
        return train;
    }

    @GetMapping(value = "/train/{id}")
    public Train hello(@PathVariable int id, @RequestParam String sort) {
        System.out.println(sort);
        Train train = new Train();
        train.setDestination("Labas");
        train.setNumero(id);
        return train;
    }
}
