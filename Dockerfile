FROM openjdk:11-jre-slim
ADD target/*.jar /app.jar
CMD ["java","-jar","/app.jar"]